# `heap-console` 🔎

> Debugging - the classic mystery game where you are the detective, the victim,
> and the murderer!
>
> *Some poor developer*

Quite a common approach in Haskell for debugging programs is to use
`Debug.Trace` - problem is that what we often want to do is not to print random
strings, but to inspect the state of the program. Debuggers are meant for this,
but they can be tricky to set up, may require speicific graphical tools to be
somewhat convenient, and it's easy to forget to run your program through one
while iterating through changes in code.

`heap-console` let's you inspect your values at runtime using simple
combinators, resuming execution once you close it:

```txt
> import Heap.Console
> inspect (42, 'a')
[Entering heap-view - use `:help` for more information]
heap-console> it
(_, 'a')
heap-console> !it
(42, 'a')
heap-console> it.1
'a'
heap-console> :exit
[Exiting heap-view]
(42, 'a')
```

If you give it `Data` instance, it can recognize records too:

```txt
> :set -XDeriveDataTypeable
> data Foo = Foo{ bar :: Int, baz :: String } deriving Data
> inspectAD $ Foo 65 "Hello World!"
[Entering heap-view - use `:help` for more information]
heap-console> !it
Foo {bar = 65, baz = "Hello World!"}
heap-console> it.baz
"Hello World!"
heap-console> ^D
[Exiting heap-view]
```

And you can bind values for inspection in random places, getting to them once
it makes sense:

```txt
> evidenceD "foo" [1,2,3]
> inspection
[Entering heap-view - use `:help` for more information]
heap-console> foo
[1, 2, 3]
heap-console> ^D
[Exiting heap-view]
```

To see more features, take a look at documentation, or simply try `:help`
command.
