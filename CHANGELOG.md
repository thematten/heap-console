# Revision history for heap-console

## 0.1.0.0 -- 2020-11-20

* First version. Released on an unsuspecting world.

## 0.1.0.1 -- 2020-11-21

* Small fixes

## (unreleased) 0.2.0.0 -- TODO

* Improved documentation
* Console now preserves state when opened multiple times during program's
  execution
* State of the console can now be saved and loaded from `.heap-console` files -
  console will automatically search for config in parent directories and load
  it on first start, while changed configuration can be saved to same file
  using the `:saveConfig` command
* New aliases for existing commands are now available (see `:help`)
* `Heap.Console.Value`: `index` now uses more precise `Index`
