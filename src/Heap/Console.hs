{-# language AllowAmbiguousTypes #-}

-- | This module provides combinators for spawning heap console in convenient
-- way.
--
-- = Console usage
--
-- Console startup is indicated by a message:
--
-- @
-- [Entering heap-view - use `:help` for more information]
-- @
--
-- followed by console's prompt:
--
-- @
-- heap-console>
-- @
--
-- here you can probe for values of bindings or use provided commands - e.g.
-- when opening console with:
--
-- @
-- inspect (42, \'a\')
-- @
--
-- you can inspect given value under name @it@:
--
-- @
-- heap-console> it
-- (_, \'a\')
-- @
--
-- or see the value strictly evaluated (up to the configured depth):
--
-- @
-- heap-console> !it
-- (42, \'a\')
-- @
--
-- or you can access it's parts by using selection:
--
-- @
-- heap-console> it.1
-- \'a\'
-- @
--
-- __Bindings__ are values bound under concrete names in console. They can be
-- automatically created with functions like 'inspectD', added in arbitrary
-- places in program using e.g. 'evidenceD' or added in console directly by
-- assigning results of selections:
--
-- @
-- heap-console> foo = bar.0.baz
-- @
--
-- __Selections__ consist of sequence of dot-separated indexes, optionally
-- prefixed with @!@ to force thunks along the way. Valid indexes are:
--
-- * positive integer (e.g. @3@) - position of element in list, tuple or other
--   data constructor
--
-- * record field name (e.g. @foo@) - name of field in record (only works when
--   given enough information - that is, when value has 'Data' instance
--   available)
--
-- In general, it's recommended to prefer combinators suffixed with @D@ when
-- possible - they require 'Data' instance for bindings being added, but
-- provide ability to recover record syntax and information about unpacked
-- fields - in case of combinators without @D@, unpacked fields appear as plain
-- @Word#@s without any information about their origin and are not indexable.
-- 'Data' instances can be easily derived using @DeriveDataTypeable@ extension.
module Heap.Console
  {-# warning
    "\"heap-console\" is meant for debugging only\
    \ - make sure you remove it in production."
  #-}
  ( inspectD
  , inspect
  , inspectingD
  , inspecting
  , inspectAD
  , inspectA
  , investigateD
  , investigate
  , inspection
  , withInspection
  , investigation
  , withEvidenceD
  , withEvidence
  , evidenceD
  , evidence
  ) where

import Control.Applicative
import Control.Arrow hiding (first, second)
import Control.Monad.Catch
import Control.Monad.Except
import Data.Bifunctor
import Data.Char
import Data.Data
import Data.Foldable
import Data.Function
import Data.Functor
import Data.IORef
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Maybe
import GHC.Exts.Heap
import GHC.Stack
import Heap.Console.Value
import System.Console.Haskeline
import System.Directory
import System.FilePath
import System.IO.Unsafe
import Text.Read (readMaybe)

-- TODO: implement auto-completion.

-------------------------------------------------------------------------------
-- | Opens console for inspecting argument before returning it. Argument is
-- provided in console under name @it@.
--
-- Compared to 'inspect' it provides more precise inspection using 'Data' -
-- prefer this one where possible.
--
-- >>> inspectD 42
-- [Entering heap-view - use `:help` for more information]
-- heap-console> !it
-- 42
-- heap-console> :exit
-- [Exiting heap-view]
-- 42
inspectD :: Data a => a -> a
inspectD = join inspectingD

-- | Opens console for inspecting argument before returning it. Argument is
-- provided in console under name @it@.
--
-- Compared to 'inspectD' it works with any lifted value, but doesn't preserve
-- record syntax and info about unpacked fields.
--
-- >>> inspect 42
-- [Entering heap-view - use `:help` for more information]
-- heap-console> !it
-- 42
-- heap-console> :exit
-- [Exiting heap-view]
-- 42
inspect :: a -> a
inspect = join inspecting

-- | Opens console for inspecting @a@ before returning @b@. Argument @a@ is
-- provided in console under name @it@.
--
-- Compared to 'inspecting' it provides more precise inspection using 'Data' -
-- prefer this one where possible.
--
-- >>> inspectingD 42 'a'
-- [Entering heap-view - use `:help` for more information]
-- heap-console> !it
-- 42
-- heap-console> :exit
-- [Exiting heap-view]
-- 'a'
inspectingD :: Data a => a -> b -> b
inspectingD = inspectingSome . Right . Value

-- | Opens console for inspecting @a@ before returning @b@. Argument @a@ is
-- provided in console under name @it@.
--
-- Compared to 'inspectingD' it works with any lifted value, but doesn't
-- preserve record syntax and info about unpacked fields.
--
-- >>> inspecting 42 'a'
-- [Entering heap-view - use `:help` for more information]
-- heap-console> !it
-- 42
-- heap-console> :exit
-- [Exiting heap-view]
-- 'a'
inspecting :: a -> b -> b
inspecting = inspectingSome . Left . asBox

inspectingSome :: Either Box Value -> b -> b
inspectingSome v = seq $ unsafePerformIO $ setBind "it" v *> heapConsole
{-# noinline inspectingSome #-}

-- | Opens console for inspecting argument. Argument is provided in console
-- under name @it@.
--
-- Compared to 'inspectA' it provides more precise inspection using 'Data' -
-- prefer this one where possible.
--
-- >>> inspectAD 42
-- [Entering heap-view - use `:help` for more information]
-- heap-console> !it
-- 42
-- heap-console> :exit
-- [Exiting heap-view]
inspectAD :: (Data a, Applicative f) => a -> f ()
inspectAD a = inspectingD a $ pure ()

-- | Opens console for inspecting argument. Argument is provided in console
-- under name @it@.
--
-- Compared to 'inspectAD' it works with any lifted value, but doesn't preserve
-- record syntax and info about unpacked fields.
--
-- >>> inspectA 42
-- [Entering heap-view - use `:help` for more information]
-- heap-console> !it
-- 42
-- heap-console> :exit
-- [Exiting heap-view]
inspectA :: Applicative f => a -> f ()
inspectA a = inspecting a $ pure ()

-- | Opens console for inspecting argument before failing with error. Argument
-- is provided in console under name @it@.
--
-- Compared to 'investigate' it provides more precise inspection using 'Data' -
-- prefer this one where possible.
--
-- >>> investigateD 42
-- [Entering heap-view - use `:help` for more information]
-- heap-console> !it
-- 42
-- heap-console> :exit
-- [Exiting heap-view]
-- *** Exception: Heap.Console.investigateD: closed investigation
-- CallStack (from HasCallStack):
--   investigateD, called at <interactive>:1:1 in interactive:Ghci1
investigateD :: (HasCallStack, Data a) => a -> b
investigateD a = withFrozenCallStack $ inspectingD a $
  error "Heap.Console.investigateD: closed investigation"

-- | Opens console for inspecting argument before failing with error. Argument
-- is provided in console under name @it@.
--
-- Compared to 'investigateD' it works with any lifted value, but doesn't
-- preserve record syntax and info about unpacked fields.
--
-- >>> investigate 42
-- [Entering heap-view - use `:help` for more information]
-- heap-console> !it
-- 42
-- heap-console> :exit
-- [Exiting heap-view]
-- *** Exception: Heap.Console.investigate: closed investigation
-- CallStack (from HasCallStack):
--   investigate, called at <interactive>:1:1 in interactive:Ghci1
investigate :: HasCallStack => a -> b
investigate a = withFrozenCallStack $ inspecting a $
  error "Heap.Console.investigate: closed investigation"

-- | Opens console with recorded "evidence" in scope.
--
-- >>> inspection
-- [Entering heap-view - use `:help` for more information]
-- ...
-- heap-console> :exit
-- [Exiting heap-view]
inspection :: Applicative f => f ()
inspection = withInspection $ pure ()

-- | Opens console with recorded "evidence" in scope, before returning given
-- argument.
--
-- >>> withInspection 42
-- [Entering heap-view - use `:help` for more information]
-- ...
-- heap-console> :exit
-- [Exiting heap-view]
-- 42
withInspection :: a -> a
-- NOTE: do not eta-reduce - GHC seems to memoize it as CAF in that case.
withInspection a = unsafePerformIO heapConsole `seq` a
{-# noinline withInspection #-}

-- | Opens console with recorded "evidence" in scope before failing with error.
--
-- >>> investigation
-- [Entering heap-view - use `:help` for more information]
-- ...
-- heap-console> :exit
-- [Exiting heap-view]
-- *** Exception: Heap.Console.investigation: closed investigation
-- CallStack (from HasCallStack):
--   investigation, called at <interactive>:1:1 in interactive:Ghci1
investigation :: HasCallStack => a
investigation = withFrozenCallStack $ withInspection $
  error "Heap.Console.investigation: closed investigation"

-- | Records @a@ as "evidence" to be later provided in console under given
-- name, before returning @b@.
--
-- Compared to 'withEvidence' it provides more precise inspection using
-- 'Data' - prefer this one where possible.
--
-- >>> withEvidenceD "foo" 'a' inspection
-- [Entering heap-view - use `:help` for more information]
-- heap-console> foo
-- 'a'
-- heap-console> :exit
-- [Exiting heap-view]
withEvidenceD :: Data a => String -> a -> b -> b
withEvidenceD n = withSomeBind n . Right . Value

-- | Records @a@ as "evidence" to be later provided in console under given
-- name, before returning @b@.
--
-- Compared to 'withEvidenceD' it works with any lifted value, but doesn't
-- preserve record syntax and info about unpacked fields.
--
-- >>> withEvidence "foo" 'a' inspection
-- [Entering heap-view - use `:help` for more information]
-- heap-console> foo
-- 'a'
-- heap-console> :exit
-- [Exiting heap-view]
withEvidence :: String -> a -> b -> b
withEvidence n = withSomeBind n . Left . asBox

-- | Records @a@ as "evidence" to be later provided in console under given
-- name.
--
-- Compared to 'evidence' it provides more precise inspection using 'Data' -
-- prefer this one where possible.
--
-- >>> evidenceD "foo" 42
-- >>> inspection
-- [Entering heap-view - use `:help` for more information]
-- heap-console> foo
-- 42
-- heap-console> :exit
-- [Exiting heap-view]
evidenceD :: (Data a, Applicative f) => String -> a -> f ()
evidenceD n v = withEvidenceD n v $ pure ()

-- | Records @a@ as "evidence" to be later provided in console under given
-- name.
--
-- Compared to 'evidenceD' it works with any lifted value, but doesn't preserve
-- record syntax and info about unpacked fields.
--
-- >>> evidence "foo" 42
-- >>> inspection
-- [Entering heap-view - use `:help` for more information]
-- heap-console> foo
-- 42
-- heap-console> :exit
-- [Exiting heap-view]
evidence :: Applicative f => String -> a -> f ()
evidence n v = withEvidence n v $ pure ()

-------------------------------------------------------------------------------
data Console = Console{
    consoleConfPath   :: Maybe FilePath
  , consoleRepOptions :: RepOptions
  , consolePrompt     :: String
  , consoleClean      :: Bool
  , consoleBinds      :: Map String (Either Box Value)
  } deriving stock Show

defaultConsole :: Console
defaultConsole =
  Console Nothing (RepOptions 16 False False) "heap-console> " False M.empty

pattern HeapConsoleConf :: String
pattern HeapConsoleConf = ".heap-console"

parseConsoleConf :: String -> Either String (Console -> Console)
parseConsoleConf =
  foldl' (\f -> liftA2 (.) f . go . parseCommandLine) (Right id) . lines
 where
  go = (=<<) \case
    [o, "=", v] -> M.lookup o options & \case
      Nothing -> Left $ "there's no option `" ++ o ++ "`"
      Just l  -> case set l v of
        Nothing -> Left $ "invalid value for option `" ++ o ++ "`"
        Just f  -> Right f
    _ -> Left "expecting settings in form `OPTION = VALUE`"

unsafeConsole :: IORef Console
unsafeConsole = unsafePerformIO do
  r <- newIORef defaultConsole
  currentToRootDirectory >>=
    flip findFile HeapConsoleConf >>= maybe (pure ()) \cp -> do
      putStrLn $ "[Reading configuration at `" ++ cp ++ "`]"
      parseConsoleConf <$> readFile cp >>= \case
        Left  e -> putStrLn $ "error while reading `" ++ cp ++ "`: " ++ e
        Right f -> atomicModifyIORef' r $
          (,()) . f . \c -> c{ consoleConfPath = Just cp }
  pure r
{-# noinline unsafeConsole #-}

getConsole :: MonadIO m => m Console
getConsole = liftIO $ readIORef unsafeConsole

modifyConsole :: MonadIO m => (Console -> Console) -> m ()
modifyConsole f = liftIO $ atomicModifyIORef' unsafeConsole $ (,()) . f

getBinds :: MonadIO m => m (Map String (Either Box Value))
getBinds = consoleBinds <$> getConsole

setBind :: MonadIO m => String -> Either Box Value -> m ()
setBind n v = modifyConsole \c ->
  c{ consoleBinds = M.insert n v $ consoleBinds c }

withSomeBind :: String -> Either Box Value -> a -> a
withSomeBind n = seq . unsafePerformIO . setBind n
{-# noinline withSomeBind #-}

-------------------------------------------------------------------------------
type ConsoleM = InputT IO

data ConsoleExit = ConsoleExit
  deriving stock Show
  deriving anyclass Exception

runConsoleM :: ConsoleM a -> IO (Maybe a)
runConsoleM =
  handle (\ConsoleExit -> pure Nothing) . fmap Just . runInputT defaultSettings

exitConsole :: ConsoleM a
exitConsole = throwM ConsoleExit

liftRepM :: RepM a -> ConsoleM (Either String a)
liftRepM ma = liftIO . runRepM ma . consoleRepOptions =<< getConsole

withRepM :: RepM a -> (a -> ConsoleM ()) -> ConsoleM ()
withRepM ma f = liftRepM ma >>= either errorC f

prompt :: ConsoleM String
prompt = maybe exitConsole pure =<< getInputLine . consolePrompt =<< getConsole

errorC :: String -> ConsoleM ()
errorC = outputStrLn . ("error: " ++)

catchInterrupt :: ConsoleM () -> ConsoleM ()
catchInterrupt = handleInterrupt (pure ()) . withInterrupt

heapConsole :: IO ()
heapConsole = do
  putStrLn "[Entering heap-view - use `:help` for more information]"
  _ <- runConsoleM $ forever $ catchInterrupt handleCommand
  shouldClean <- consoleClean <$> getConsole
  when shouldClean $ modifyConsole \c -> c{ consoleBinds = M.empty }
  putStrLn "[Exiting heap-view]"

-------------------------------------------------------------------------------
handleCommand :: ConsoleM ()
handleCommand = parseInputCommand <$> prompt >>= \case
  Left  e -> errorC e
  Right ic -> case ic of
    Empty -> pure ()
    Bind n s -> withSelected s \v -> setBind n v
    ViewSelection s -> withSelected s \v -> withRepM (prettyRep v) outputStrLn
    PrefixCommand n as -> case M.lookup n commandMap of
      Nothing -> errorC $ "unknown command `:" ++ n ++ "`"
      Just c  -> fromMaybe (errorC $ unexpectedArgs c) $ commandAction c as

data InputCommand
  = Empty
  | PrefixCommand String [String]
  | Bind String Selection
  | ViewSelection Selection
  deriving stock Show

data Selection = Selection Bool String [Index] deriving stock Show

parseInputCommand :: String -> Either String InputCommand
parseInputCommand str = parseCommandLine str >>= \case
  []             -> Right Empty
  (':':cmd):args -> Right $ PrefixCommand cmd args
  [n, "=", s]    -> if isIdentifier n
    then Bind n <$> parseSelection s
    else Left $ "`" ++ n ++ "` isn't a valid binding name"
  [s]            -> ViewSelection <$> parseSelection s
  _ -> Left "couldn't parse input - expecting command, binding or selection"

parseSelection :: String -> Either String Selection
parseSelection = \case
  '!':cs -> uncurry (Selection True)  <$> go cs
  cs     -> uncurry (Selection False) <$> go cs
 where
  go = groupBy (\_ c -> c /= '.') >>> \case
    []   -> Left "missing selection"
    n:is -> if isIdentifier n
      then (n,) <$> traverse (parseIndex . tail) is
      else Left $ "`" ++ n ++ "` isn't a valid binding name"

parseIndex :: String -> Either String Index
parseIndex s
  | Just i <- readMaybe s = Right $ NumIndex i
  | isIdentifier s        = Right $ FieldIndex s
  | otherwise             = Left $ "`" ++ s ++ "` isn't a valid index"

data Command = Command{
    commandNames       :: [String]
  , commandArgs        :: [String]
  , commandDescription :: [String]
  , commandAction      :: [String] -> Maybe (ConsoleM ())
  }

showHelp :: [Command] -> String
showHelp cs = unlines
  $ "Usage:"
  : ""
  : "  [!]SELECTION"
  : "    Prints selection [strictly]."
  : "  BINDING = SELECTION"
  : "    Binds result of SELECTION to BINDING."
  : commandsHelp cs
 where
  commandsHelp = concatMap \c ->
    ("  " ++ showUsage c) : (("    " ++) <$> commandDescription c)

unexpectedArgs :: Command -> String
unexpectedArgs c =
  "unexpected arguments - usage: `" ++ showUsage c ++ "`"

showUsage :: Command -> String
showUsage c = case args of
  _:_ | _:_:_ <- commandNames c ->
    '(' : names ++ ") " ++ intercalate " " args
  _ -> intercalate " " $ names:args
 where
  names = intercalate " | " $ map (':' :) $ commandNames c
  args  = commandArgs c

commands :: [Command]
commands =
  [ Command ["help", "usage"] [] ["Shows this text."] \_ ->
      Just $ outputStrLn $ showHelp commands
  , Command ["exit", "quit", "q"] []
      ["Returns back to program (same as <ctrl-D>)."]
      \_ -> Just exitConsole
  , Command ["show"] ["[OPTION]"]
      [ "Shows value of a selected option, or values of all options if not given any."
      , "Available options:"
      -- TODO: move descriptions of options to 'Option'?
      , "  depth :: Natural"
      , "    depth of printed representation"
      , "  showTypes :: Bool"
      , "    whether to show types in printed representation"
      , "  prompt :: String"
      , "    console prompt"
      , "  strict :: Bool"
      , "    whether inspection should always force values along the way"
      , "  cleanOnExit :: Bool"
      , "    whether bindings should be cleaned up on exit"
      ]
      \case
        [o] -> Just $ withOption o \l -> outputStrLn . view l =<< getConsole
        []  -> Just do
          c <- getConsole
          for_ (M.toList options) \(o, l) ->
            outputStrLn $ o ++ " = " ++ view l c
        _ -> Nothing
  , Command ["set"] ["OPTION", "VALUE"] ["Changes option to given value."]
      \case
        [o, v] -> Just $ withOption o \l -> case set l v of
          Nothing -> errorC $ "invalid value for option `" ++ o ++ "`"
          Just f  -> modifyConsole f
        _ -> Nothing
  , Command ["info", "i"] ["SELECTION"] ["Prints info about selected value."]
      \case
        [parseSelection -> Right s] -> Just $ withSelected s $
          outputStrLn . show <=< liftIO .
            either getBoxedClosureData \(Value v) -> getClosureData v
        _ -> Nothing
  , Command ["binds", "bindings"] [] ["Lists bindings in scope."] \case
      [] -> Just $ getBinds >>= traverse_ outputStrLn . M.keys
      _ -> Nothing
  , Command ["saveConfig"] []
      [ "Saves configuration to the closest `.heap-console` file or creates a new one next"
      , "to the executable."
      ]
      \case
        [] -> Just do
          c  <- getConsole
          cp <- maybe (liftIO getCurrentDirectory <&> (</> HeapConsoleConf)) pure $
            consoleConfPath c
          liftIO $ writeFile cp $ unlines $ M.toList options <&> \(o, l) ->
            o ++ " = " ++ view l c
          outputStrLn $ "Saved configuration to `" ++ cp ++ "`."
        _ -> Nothing
  ]

commandMap :: Map String Command
commandMap = foldl' addCommand M.empty commands where
  addCommand acc c = foldl' (\m n -> M.insert n c m) acc $ commandNames c

withBind :: String -> (Either Box Value -> ConsoleM ()) -> ConsoleM ()
withBind n f = maybe (errorC $ "binding `" ++ n ++ "` not in scope") f .
  M.lookup n =<< getBinds

withSelected :: Selection -> (Either Box Value -> ConsoleM ()) -> ConsoleM ()
withSelected (Selection s n is) f =
  withBind n \v -> withRepM (index v s is) f

withOption :: String -> (Option Console -> ConsoleM ()) -> ConsoleM ()
withOption o f =
  maybe (errorC $ "there's no option `" ++ o ++ "`") f $ M.lookup o options

options :: Map String (Option Console)
options = M.fromList
  [ ( "depth"
    , option (repDepth . consoleRepOptions) \repDepth c ->
        c{ consoleRepOptions = (consoleRepOptions c){ repDepth } }
    )
  , ( "showTypes"
    , option (repTypes . consoleRepOptions) \repTypes c ->
        c{ consoleRepOptions = (consoleRepOptions c){ repTypes } }
    )
  , ( "prompt"
    , Option (show . consolePrompt) \consolePrompt ->
        Just \c -> c{ consolePrompt }
    )
  , ( "strict"
    , option (repStrict . consoleRepOptions) \repStrict c ->
        c{ consoleRepOptions = (consoleRepOptions c){ repStrict } }
    )
  , ( "cleanOnExit"
    , option consoleClean \consoleClean c -> c{ consoleClean }
    )
  ]

-------------------------------------------------------------------------------
data Option a = Option{ view :: a -> String, set :: String -> Maybe (a -> a) }

option :: (Show a, Read a) => (x -> a) -> (a -> x -> x) -> Option x
option f t = Option (show . f) $ fmap t . readMaybe

-------------------------------------------------------------------------------
currentToRootDirectory :: IO [FilePath]
currentToRootDirectory =
  reverse . map joinPath . tail . inits . splitPath <$> getCurrentDirectory

-------------------------------------------------------------------------------
isIdentifier :: String -> Bool
isIdentifier = \case
  []   -> False
  c:cs -> (isAlpha c || c == '_') && all (\x -> isAlphaNum x || x == '_') cs

parseCommandLine :: String -> Either String [String]
parseCommandLine = goSpaced where
  goSpaced = \case
    [] -> Right []
    cs -> do
      (x, cs') <- goLexeme False (dropWhile isSpace cs)
      (x :) <$> goSpaced cs'
  goLexeme q = \case
    "" | not q     -> Right ([], [])
       | otherwise -> Left "unexpected end of line, expected quote '\"'"
    '\\':'"':cs    -> first ('"':) <$> goLexeme q cs
    '"':cs         -> goLexeme (not q) cs
    ' ':cs | not q -> Right ([], cs)
    c:cs           -> first (c:) <$> goLexeme q cs
